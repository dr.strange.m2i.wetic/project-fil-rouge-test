package com.wetic.jersey.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "facture")
public class Facture implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date_facturation", nullable = false)
	private ZonedDateTime dateFacturation;

	@ManyToOne
	@JoinColumn(unique = true)
	private Client client;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDateFacturation() {
		return dateFacturation;
	}

	public Facture dateFacturation(ZonedDateTime dateFacturation) {
		this.dateFacturation = dateFacturation;
		return this;
	}

	public void setDateFacturation(ZonedDateTime dateFacturation) {
		this.dateFacturation = dateFacturation;
	}

	// @ManyToOne
	// @JsonIgnoreProperties("factures")
	// private Client client;

	@OneToMany(mappedBy = "facture")

	private Set<DetailsFacture> detailsFactures = new HashSet<>();

	// public Client getClient() {
	// return client;
	// }
	//
	// public void setClient(Client client) {
	// this.client = client;
	// }

	// public Facture client(Client client) {
	// this.client = client;
	// return this;
	// }

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Facture)) {
			return false;
		}
		return id != null && id.equals(((Facture) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Facture{" + "id=" + getId() + ", dateFacturation='" + getDateFacturation() + "'" + "}";
	}

}
