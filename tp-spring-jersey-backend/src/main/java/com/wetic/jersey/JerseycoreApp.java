package com.wetic.jersey;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

import com.wetic.jersey.config.ApplicationProperties;
import com.wetic.jersey.config.DefaultProfileUtil;
import com.wetic.jersey.domain.Client;
import com.wetic.jersey.domain.Depense;
import com.wetic.jersey.domain.DetailsFacture;
import com.wetic.jersey.domain.Facture;
import com.wetic.jersey.domain.Produit;
import com.wetic.jersey.domain.TypeDepense;
import com.wetic.jersey.domain.User;
import com.wetic.jersey.repository.ClientRepository;
import com.wetic.jersey.repository.DepenseRepository;
import com.wetic.jersey.repository.DetailsFactureRepository;
import com.wetic.jersey.repository.FactureRepository;
import com.wetic.jersey.repository.ProduitRepository;
import com.wetic.jersey.repository.TypeDepenseRepository;
import com.wetic.jersey.repository.UserRepository;

import io.github.jhipster.config.JHipsterConstants;

@SpringBootApplication
@EnableConfigurationProperties({ LiquibaseProperties.class, ApplicationProperties.class })
public class JerseycoreApp implements InitializingBean, CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(JerseycoreApp.class);

	private final Environment env;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private DepenseRepository depenseRepository;
	@Autowired
	private FactureRepository factureRepository;
	@Autowired
	private DetailsFactureRepository detailsFactureRepository;
	@Autowired
	private TypeDepenseRepository typeDepenseRepository;

	public JerseycoreApp(Environment env) {
		this.env = env;
	}

	/**
	 * Initializes jerseycore.
	 * <p>
	 * Spring profiles can be configured with a program argument
	 * --spring.profiles.active=your-active-profile
	 * <p>
	 * You can find more information on how profiles work with JHipster on <a href=
	 * "https://www.jhipster.tech/profiles/">https://www.jhipster.tech/profiles/</a>.
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
		if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
				&& activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
			log.error("You have misconfigured your application! It should not run "
					+ "with both the 'dev' and 'prod' profiles at the same time.");
		}
		if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
				&& activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
			log.error("You have misconfigured your application! It should not "
					+ "run with both the 'dev' and 'cloud' profiles at the same time.");
		}
	}

	/**
	 * Main method, used to run the application.
	 *
	 * @param args
	 *            the command line arguments.
	 */
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(JerseycoreApp.class);
		DefaultProfileUtil.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		logApplicationStartup(env);
	}

	private static void logApplicationStartup(Environment env) {
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		String serverPort = env.getProperty("server.port");
		String contextPath = env.getProperty("server.servlet.context-path");
		if (StringUtils.isBlank(contextPath)) {
			contextPath = "/";
		}
		String hostAddress = "localhost";
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.warn("The host name could not be determined, using `localhost` as fallback");
		}
		log.info(
				"\n----------------------------------------------------------\n\t"
						+ "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}{}\n\t"
						+ "External: \t{}://{}:{}{}\n\t"
						+ "Profile(s): \t{}\n----------------------------------------------------------",
				env.getProperty("spring.application.name"), protocol, serverPort, contextPath, protocol, hostAddress,
				serverPort, contextPath, env.getActiveProfiles());
	}

	public void run(String... args) throws Exception {

		User user = new User();
		user.setLogin("admin1");
		user.setEmail("admin1@gmail.com");
		user.setPassword("e00cf25ad42683b3df678c61f42c6bda");
		user.setFirstName("Jean");
		user.setLastName("Jacques");
		User user2 = new User();
		user2.setLogin("admin2");
		user2.setEmail("admin2@gmail.com");
		user2.setActivated(true);
		user2.setPassword("e00cf25ad42683b3df678c61f42c6bde");
		user2.setFirstName("Jean");
		user2.setLastName("Claude");
		Client client = new Client();
		client.setAdresse("rue hell");
		client.setCategorie("PL");
		client.setCompte("666 account");
		client.setLocalite("ville test");
		client.setUser(user);
		Client client2 = new Client();
		client2.setAdresse("rue hell2");
		client2.setCategorie("PL2");
		client2.setCompte("666 account 2");
		client2.setLocalite("ville test v2");
		client2.setUser(user2);
		// run produits
		Produit produit = new Produit();
		produit.setLibelle("peluche licorne");
		produit.setDescription("l'animal fantastique préféré des enfants !");
		produit.setPrix(30);
		produit.setQteStock(12);
		Produit produit2 = new Produit();
		produit2.setLibelle("peluche guillotine");
		produit2.setDescription("la peluche du petit révolutionnaire !");
		produit2.setPrix(20);
		produit2.setQteStock(8);
		// run depenses
		ZonedDateTime d = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Europe/Paris"));
		Depense depense = new Depense();
		depense.setDate(d);
		depense.setDescription("desc Test 1");
		depense.setLibelle("Libelle Test 1");
		depense.setMontant((float) 1578);
		depense.setTypeDepense(null);
		depenseRepository.save(depense);
		TypeDepense typeDepense = new TypeDepense();
		typeDepense.setDescription("desc Type depense test 1");
		typeDepense.setLibelle("libelle type depense test 1");
		// run facture
		Facture facture = new Facture();
		facture.setDateFacturation(d);
		// run details
		DetailsFacture detailsFacture = new DetailsFacture();
		detailsFacture.setDescription("Desc test");
		detailsFacture.setFacture(null);
		detailsFacture.setQteProduit(200);

		userRepository.save(user);
		userRepository.save(user2);
		clientRepository.save(client);
		clientRepository.save(client2);
		produitRepository.save(produit);
		produitRepository.save(produit2);
		depenseRepository.save(depense);
		typeDepenseRepository.save(typeDepense);
		factureRepository.save(facture);
		detailsFactureRepository.save(detailsFacture);

	}
}
