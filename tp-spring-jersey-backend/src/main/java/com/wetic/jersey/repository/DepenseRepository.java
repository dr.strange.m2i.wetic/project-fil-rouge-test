package com.wetic.jersey.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.wetic.jersey.domain.Depense;

@SuppressWarnings("unused")
@Repository
public interface DepenseRepository extends JpaRepository<Depense, Long> {
	
	@Override
	@Query(value="select d.id, d.datefacturation, d.description, d.libelle, d.montant, td.libelle from depense d join typedepense td  on d.typedepense_id = td.id",nativeQuery=true)
	public Page<Depense>findAll(Pageable pageable);
	
	@Query(value="select sum(montant) from depense",nativeQuery=true)
	public Float SumAll(Float montant);
	

}
