package com.wetic.jersey.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.wetic.jersey.domain.TypeDepense;

@SuppressWarnings("unused")
@Repository
public interface TypeDepenseRepository extends JpaRepository<TypeDepense, Long> {
	
	}
