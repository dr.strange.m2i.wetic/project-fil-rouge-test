package com.wetic.jersey.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import com.wetic.jersey.domain.Produit;

@SuppressWarnings("unused")
@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long>{

	// ayyyy: Récupérer Le nombre total des produits

//	@Query(value="select SUM(qte_stock) from produit",nativeQuery=true)
//	public int findTotalQuantityOfProducts();
	
	@Query(value="select COUNT(*) from produit",nativeQuery=true)
	public int findTotalNbOfProducts();
}
