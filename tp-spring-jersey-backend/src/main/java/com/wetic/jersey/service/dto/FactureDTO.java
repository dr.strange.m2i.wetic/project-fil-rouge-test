package com.wetic.jersey.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.validation.constraints.NotNull;

public class FactureDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	private ZonedDateTime dateFacturation;
	
	@NotNull
	private Long clientId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDateFacturation() {
		return dateFacturation;
	}

	public void setDateFacturation(ZonedDateTime dateFacturation) {
		this.dateFacturation = dateFacturation;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientId == null) ? 0 : clientId.hashCode());
		result = prime * result + ((dateFacturation == null) ? 0 : dateFacturation.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FactureDTO other = (FactureDTO) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (dateFacturation == null) {
			if (other.dateFacturation != null)
				return false;
		} else if (!dateFacturation.equals(other.dateFacturation))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FactureDTO {id=" + getId() + ", dateFacturation=" + getDateFacturation() + ", clientId=" + getClientId() + "}";
	}
	
	
	
}
