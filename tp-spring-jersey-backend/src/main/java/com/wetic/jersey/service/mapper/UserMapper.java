package com.wetic.jersey.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wetic.jersey.domain.User;
import com.wetic.jersey.service.dto.UserDTO;

/**
 * Mapper for the entity {@link User} and its DTO {@link UserDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserMapper extends EntityMapper<UserDTO, User> {
	@Mapping(source = "user.id", target = "userId")
	UserDTO toDto(User user);

	@Mapping(source = "userId", target = "jhi_user")
	User toEntity(UserDTO UserDTO);

	default User fromId(Long id) {
		if (id == null) {
			return null;
		}
		User user = new User();
		user.setId(id);
		return user;
	}
}
