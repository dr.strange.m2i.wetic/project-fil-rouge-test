package com.wetic.jersey.service.mapper;

import org.mapstruct.Mapping;

import com.wetic.jersey.domain.DetailsFacture;
import com.wetic.jersey.service.dto.DetailsFactureDTO;


public interface DetailsFactureMapper extends EntityMapper<DetailsFactureDTO, DetailsFacture> {
	@Mapping(source = "facture.id", target = "factureId")
    @Mapping(source = "produit.id", target = "produitId")
    DetailsFactureDTO toDto(DetailsFacture detailsFacture);

    @Mapping(source = "workoutId", target = "workout")
    @Mapping(source = "exerciseId", target = "exercise")
    DetailsFacture toEntity(DetailsFactureDTO detailsFactureDTO);

    default DetailsFacture fromId(Long id) {
        if (id == null) {
            return null;
        }
        DetailsFacture detailsFacture = new DetailsFacture();
        detailsFacture.setId(id);
        return detailsFacture;
    }
}
