package com.wetic.jersey.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wetic.jersey.domain.Facture;
import com.wetic.jersey.service.dto.FactureDTO;

/**
 * Mapper for the entity {@link Facture} and its DTO {@link FactureDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class})
public interface FactureMapper extends EntityMapper<FactureDTO , Facture>{
	 @Mapping(source = "client.id", target = "clientId")
	    FactureDTO toDto(Facture facture);

	    @Mapping(target = "detailsFactures", ignore = true)
	    @Mapping(source = "clientId", target = "client")
	    Facture toEntity(FactureDTO factureDTO);

	    default Facture fromId(Long id) {
	        if (id == null) {
	            return null;
	        }
	        Facture facture = new Facture();
	        facture.setId(id);
	        return facture;
	    }
}
