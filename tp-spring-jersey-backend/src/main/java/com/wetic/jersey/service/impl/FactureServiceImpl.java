package com.wetic.jersey.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wetic.jersey.domain.Facture;
import com.wetic.jersey.repository.FactureRepository;
import com.wetic.jersey.service.FactureService;
import com.wetic.jersey.service.dto.FactureDTO;
import com.wetic.jersey.service.mapper.FactureMapper;

public class FactureServiceImpl implements FactureService{
	
	private final FactureRepository factureRepository;
	
	private final FactureMapper factureMapper;
	
	public FactureServiceImpl(FactureRepository factureRepository, FactureMapper factureMapper) {
		this.factureMapper = factureMapper;
		this.factureRepository = factureRepository;
	}

	@Override
	public FactureDTO save(FactureDTO factureDTO) {
		Facture facture = factureMapper.toEntity(factureDTO);
		facture = factureRepository.save(facture);
		return factureMapper.toDto(facture);
	}

	@Override
	public Page<FactureDTO> findAll(Pageable pageable) {
		return factureRepository.findAll(pageable).map(factureMapper::toDto);
	}

	@Override
	public Optional<FactureDTO> findOne(Long id) {
		return factureRepository.findById(id).map(factureMapper::toDto);
	}
	
	@Override
	public Page<FactureDTO> findByClient(Pageable pageable , Long id){
		return factureRepository.findByClient(pageable, id).map(factureMapper::toDto);
	}
	
	public Long countAll() {
		return factureRepository.CountAll();
	}

	@Override
	public void delete(Long id) {
		factureRepository.deleteById(id);		
	}

	
}
